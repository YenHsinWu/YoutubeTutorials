#include <stdio.h>

int main(){
    int grade;
    char rank;
    printf("Please input your grade (between 0-100): ");
    scanf("%d", &grade);

    if(grade > 90)
        rank = 'A';
    else if(grade > 80)
        rank = 'B';
    else if(grade > 60)
        rank = 'C';
    else
        rank = 'F';

    printf("Your rank is %c\n", rank);

    return 0;
}