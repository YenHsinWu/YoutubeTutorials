#include <stdio.h>

/*

邏輯運算子共有三種： &&(且), ||(或), !(非)
真值表：

&&    |  true    false
----------------------
true  |  true    false
false |  false   false

||    |  true    false
----------------------
true  |  true    true
false |  true    false

!     |  true    false
----------------------
      |  false   true

*/

int main(){
    int a, b;

    a = 0;
    b = 0;
    printf("--------------------------\n");
    printf("a = 0, b = 0\n");
    printf("a 且 b = %d\n", a && b);
    printf("a 或 b = %d\n", a || b);
    printf("非a = %d, 非b = %d\n", !a, !b);

    a = 1;
    b = 0;
    printf("--------------------------\n");
    printf("a = 1, b = 0\n");
    printf("a 且 b = %d\n", a && b);
    printf("a 或 b = %d\n", a || b);
    printf("非a = %d, 非b = %d\n", !a, !b);

    a = 0;
    b = 1;
    printf("--------------------------\n");
    printf("a = 0, b = 1\n");
    printf("a 且 b = %d\n", a && b);
    printf("a 或 b = %d\n", a || b);
    printf("非a = %d, 非b = %d\n", !a, !b);

    a = 1;
    b = 1;
    printf("--------------------------\n");
    printf("a = 1, b = 1\n");
    printf("a 且 b = %d\n", a && b);
    printf("a 或 b = %d\n", a || b);
    printf("非a = %d, 非b = %d\n", !a, !b);

    return 0;
}