#include <stdio.h>

/*

There are three types of logical operators: &&(and), ||(or), !(not)
Truth table:

&&    |  true    false
----------------------
true  |  true    false
false |  false   false

||    |  true    false
----------------------
true  |  true    true
false |  true    false

!     |  true    false
----------------------
      |  false   true

*/

int main(){
    int a, b;

    a = 0;
    b = 0;
    printf("--------------------------\n");
    printf("a = 0, b = 0\n");
    printf("a and b = %d\n", a && b);
    printf("a or b = %d\n", a || b);
    printf("not a = %d, not b = %d\n", !a, !b);

    a = 1;
    b = 0;
    printf("--------------------------\n");
    printf("a = 1, b = 0\n");
    printf("a and b = %d\n", a && b);
    printf("a or b = %d\n", a || b);
    printf("not a = %d, not b = %d\n", !a, !b);

    a = 0;
    b = 1;
    printf("--------------------------\n");
    printf("a = 0, b = 1\n");
    printf("a and b = %d\n", a && b);
    printf("a or b = %d\n", a || b);
    printf("not a = %d, not b = %d\n", !a, !b);

    a = 1;
    b = 1;
    printf("--------------------------\n");
    printf("a = 1, b = 1\n");
    printf("a and b = %d\n", a && b);
    printf("a or b = %d\n", a || b);
    printf("not a = %d, not b = %d\n", !a, !b);

    return 0;
}