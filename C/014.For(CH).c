#include <stdio.h>

/*
for(初始設定; 判斷條件; 迴圈增值){
    重複執行這段程式
}

for迴圈總共可以有2^3 = 8種
*/

int main(){
    int cnt;

    printf("請輸入計數器的值: ");
    scanf("%d", &cnt);

    for(int i = cnt; i > 0; i --){
        printf("%d ", i);
    }
    printf("\n");

    return 0;
}