#include <stdio.h>

int main(){
    int score1, score2, score3, total_score;
    double avg_score;

    printf("請輸入三次考試成績: ");
    scanf("%d %d %d", &score1, &score2, &score3);

    total_score = score1 + score2 + score3;
    avg_score = total_score / 3.0;

    printf("你的總分是: %d\n", total_score);
    printf("你的平均分數是: %f\n", avg_score);

    return 0;
}