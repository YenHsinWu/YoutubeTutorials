#include <stdio.h>

int main(){
    int level;
    printf("Please input how many level you want to print: ");
    scanf("%d", &level);

    for(int i = 0; i < level; i ++){
        for(int j = 0; j <= i; j ++){
            printf("*");
        }
        printf("\n");
    }

    return 0;
}