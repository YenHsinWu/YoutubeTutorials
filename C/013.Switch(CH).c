#include <stdio.h>

/*
switch(表達式){
    case 條件1:
        做一些事情
    case 條件2:
        做一些事情
    ...
    case 條件n:
        做一些事情
    default:
        當所有條件都不符合，執行這一塊
}
*/

int main(){
    //會員等級
    char level;
    printf("請輸入你的會員等級(A-C): ");
    scanf("%c", &level);

    switch(level){
        case 'A':
            printf("你的會員等級為金星會員\n");
            break;
        case 'B':
            printf("你的會員等級為銀星會員\n");
            break;
        case 'C':
            printf("你的會員等級為銅牌會員\n");
            break;
        default:
            printf("輸入不在格式內，錯誤\n");
            break;
    }

    return 0;
}