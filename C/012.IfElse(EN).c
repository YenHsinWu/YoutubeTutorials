#include <stdio.h>

/*
1. if(condition){
    If condition is true, run this block of codes.
    }

2. if(condition){
    If condition is true, run this block of codes.
    }
    else{
    If condition is false, run this block of codes.
    }

3. if(condition1){
    If condition1 is true, run this block of codes.
    }
    else if(condition2){
    If condition1 is false and condition2 is true, run this block of codes.
    }
    else if(condition3){
    If both condition1 and condition2 are false and condition3 is true, run this block of codes.
    }
    ...
    else{
    If all conditions are false, run this block of codes.
    }
*/

int main(){
    // Determine odd or even number.
    int num;
    printf("Please input an integer：");
    scanf("%d", &num);

    if(num % 2 == 0){
        printf("The number %d is an even number.\n", num);
    }
    else{
        printf("The number %d is an odd number.\n", num);
    }

    // Determine the discount percent by your shopping price.
    int price;
    double discount_percent;
    printf("\nPlease input your shopping price: ");
    scanf("%d", &price);

    if(price > 300){
        discount_percent = 0.20;
    }
    else if(price > 200){
        discount_percent = 0.15;
    }
    else if(price > 100){
        discount_percent = 0.10;
    }
    else{
        discount_percent = 0.0;
    }

    printf("Your discount percent is: %.1f%%\n", discount_percent * 100);

    return 0;
}

/*
Use the raw grade to determine the rank.
Let user input his/her grade(An integer between 0-100)
[100-90) is A
[90-80) is B
[80-60) is C
[60-0] is F

Output: Your rank is <rank>.
*/