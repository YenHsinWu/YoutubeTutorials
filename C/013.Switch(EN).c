#include <stdio.h>

/*
switch(expression){
    case condition 1:
        do something
    case condition 2:
        do something
    ...
    case condition n:
        do something
    default:
        Run this block when all conditions are failed
}
*/

int main(){
    // Member level
    char level;
    printf("Please enter your member level(A-C): ");
    scanf("%c", &level);

    switch(level){
        case 'A':
            printf("You have golden membership\n");
            break;
        case 'B':
            printf("You have silver membership\n");
            break;
        case 'C':
            printf("You have bronze membership\n");
            break;
        default:
            printf("Not in the format, error\n");
            break;
    }

    return 0;
}