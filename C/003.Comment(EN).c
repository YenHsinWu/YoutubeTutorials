// Edited : 2022 / 3 / 1
#include <stdio.h>
// Ya!
int main(){
    // This function will print Hello world!
    // And then create a new line!
    /* Here is also comment.
    This function will print Hello world!
    and then create a new line!
    */
    printf("Hello world!\n");

    return 0;
}

// Hello this is the end of code.