#include <stdio.h>

int main(){
    /*
    比較運算子共有6種：
    1. > 大於
    2. < 小於
    3. >= 大於或等於
    4. <= 小於或等於
    5. == 等於
    6. != 不等於
    */

    int a = 10, b = 7;

    printf("a = %d, b = %d\n", a, b);
    printf("--------------------\n");

    printf("a > b is %d\n", a > b); // 非零
    printf("a < b is %d\n", a < b); // 零
    printf("a >= b is %d\n", a >= b); // 非零
    printf("a <= b is %d\n", a <= b); // 零
    printf("--------------------\n");

    a = 12;
    b = 12;
    printf("a = %d, b = %d\n", a, b);
    printf("--------------------\n");

    printf("a >= b is %d\n", a >= b); // 非零
    printf("a <= b is %d\n", a <= b); // 非零
    printf("a == b is %d\n", a == b); // 非零
    printf("a != b is %d\n", a != b); // 零

    return 0;
}