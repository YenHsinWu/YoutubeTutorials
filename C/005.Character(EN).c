#include <stdio.h>

int main(){
    char a = 'a', b = 'b', c = 'c';

    printf("Variable a : %c\n", a);
    printf("Variable b : %c\n", b);
    printf("Variable c : %c\n", c);

    // Reference: https://en.wikipedia.org/wiki/ASCII

    printf("\n");
    printf("Let's check the codes represent the variables\n");
    printf("Code for a : %d\n", a);
    printf("Code for b : %d\n", b);
    printf("Code for c : %d\n", c);

    printf("\n");
    printf("If we have digits in char type\n");

    char num1 = '0', num2 = '1';
    printf("Variable num1 : %c\n", num1);
    printf("Variable num2 : %c\n", num2);

    printf("\n");
    printf("Let's check the codes represent the numbers\n");
    printf("Code for num1 : %d\n", num1);
    printf("Code for num2 : %d\n", num2);


    return 0;
}