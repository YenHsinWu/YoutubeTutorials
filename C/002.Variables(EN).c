#include <stdio.h>

int main(){
    int var_int = 1; // int -> integer
    char var_char = 'A'; // char -> character
    float var_float = 3.14; // float -> floating point
    double var_double = 3.1415926; // double -> double precision floating point

    printf("Initialized variables...\n");
    printf("The integer is : %d\n", var_int);
    printf("The character is : %c\n", var_char);
    printf("The float is : %f\n", var_float);
    printf("The double precision float is : %lf\n", var_double);

    var_int = 2;
    var_char = 'C';
    var_float = 2.72;
    var_double = 2.718281828;

    printf("After changing the contents in variables...\n");
    printf("The integer is : %d\n", var_int);
    printf("The character is : %c\n", var_char);
    printf("The float is : %f\n", var_float);
    printf("The double precision float is : %lf\n", var_double);

    return 0;
}
