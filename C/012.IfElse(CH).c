#include <stdio.h>

/*
1. if(條件){
    當條件為真，執行此區塊
    }

2. if(條件){
    當條件為真，執行此區塊
    }
    else{
    當條件為假，執行此區塊
    }

3. if(條件1){
    當條件1為真，執行此區塊
    }
    else if(條件2){
    當條件1為假且條件2為真，執行此區塊
    }
    else if(條件3){
    當條件1和條件2皆為假且條件3為真，執行此區塊
    }
    ...
    else{
    當所有條件皆為假，執行此區塊
    }
*/

int main(){
    // 判斷奇偶數
    int num;
    printf("請給我一個整數：");
    scanf("%d", &num);

    if(num % 2 == 0){
        printf("輸入的整數%d為偶數\n", num);
    }
    else{
        printf("輸入的整數%d為奇數\n", num);
    }

    // 滿額打折
    int price;
    double discount_percent;
    printf("\n請輸入您的消費金額：");
    scanf("%d", &price);

    if(price > 300){
        discount_percent = 0.20;
    }
    else if(price > 200){
        discount_percent = 0.15;
    }
    else if(price > 100){
        discount_percent = 0.10;
    }
    else{
        discount_percent = 0.0;
    }

    printf("您的折扣為：%.1f%%\n", discount_percent * 100);

    return 0;
}

/*
用分數判斷等第制：
請使用者輸入自己的成績(介於0-100的整數)
[100-90)分為A
[90-80)分為B
[80-60)分為C
[60-0]分為F

輸出：您的等第制為 <等第>
*/