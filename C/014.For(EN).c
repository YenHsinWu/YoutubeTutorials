#include <stdio.h>

/*
for(initializing; condition; increment){
    do this block until condition is not fulfilled.
}

for loop has 2^3 = 8 types.
*/

int main(){
    int cnt;
    
    printf("Please input the value of counter: ");
    scanf("%d", &cnt);

    for(int i = 0; i < cnt; i ++){
        printf("%d ", i);
    }
    printf("\n");

    return 0;
}