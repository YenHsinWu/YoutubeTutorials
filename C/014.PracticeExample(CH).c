#include <stdio.h>

int main(){
    int level;
    printf("請輸入要印到第幾層: ");
    scanf("%d", &level);

    for(int i = 0; i < level; i ++){
        for(int j = 0; j <= i; j ++){
            printf("*");
        }
        printf("\n");
    }

    return 0;
}