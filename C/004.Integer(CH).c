#include <stdio.h>

int main(){
    int a = 11, b = 5;

    /*
    整數有五種運算：
    1. '+' = 加法
    2. '-' = 減法
    3. '*' = 乘法
    4. '/' = 取商
    5. '%' = 取餘數
    */

    int add, pos_sub, neg_sub, mul, div, mod;

    add = a + b; // = 16
    pos_sub = a - b; // = 6
    neg_sub = b - a; // = -6
    mul = a * b; // = 55
    div = a / b; // = 2
    mod = a % b; // = 1

    printf("a + b的和 : %d\n", add);
    printf("a - b的差 : %d\n", pos_sub);
    printf("b - a的差 : %d\n", neg_sub);
    printf("a * b的積 : %d\n", mul);
    printf("a / b的商 : %d\n", div);
    printf("a / b的餘數 : %d\n", mod);
    printf("================================================\n");
    printf("現在將a / b變成b / a.............................\n");
    printf("b / a的商 : %d\n", b / a); // 5 / 11 = 0
    printf("b / a的餘數 : %d\n", b % a); // 5 % 11 = 5

    return 0;
}
