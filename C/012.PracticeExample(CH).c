#include <stdio.h>

int main(){
    int grade;
    char rank;
    printf("請輸入您的成績(介於0-100)：");
    scanf("%d", &grade);

    if(grade > 90)
        rank = 'A';
    else if(grade > 80)
        rank = 'B';
    else if(grade > 60)
        rank = 'C';
    else
        rank = 'F';

    printf("您的等第制為：%c\n", rank);

    return 0;
}