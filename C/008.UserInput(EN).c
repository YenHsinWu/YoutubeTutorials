#include <stdio.h>

int main(){
    int score1, score2, score3, total_score;
    double avg_score;

    printf("Please input three scores: ");
    scanf("%d %d %d", &score1, &score2, &score3);

    total_score = score1 + score2 + score3;
    avg_score = total_score / 3.0;

    printf("Your total score is: %d\n", total_score);
    printf("Your average score is: %f\n", avg_score);

    return 0;
}