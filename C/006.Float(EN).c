#include <stdio.h>

int main(){
    double a = 2.5, b = 3.6;

    printf("Addition : %f\n", a + b);
    printf("Substrction : %f\n", a - b);
    printf("Multiplication : %f\n", a * b);
    printf("Division : %.1f\n", a / b);

    return 0;
}