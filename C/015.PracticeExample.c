#include <stdio.h>

int main(){
    int level, i, j;
    printf("請輸入要印到第幾層: ");
    scanf("%d", &level);

    i = 0;

    while(i < level){
        j = 0;
        
        while(j <= i){
            printf("*");
            j ++;
        }

        printf("\n");
        i ++;
    }

    return 0;
}