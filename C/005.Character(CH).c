#include <stdio.h>

int main(){
    char a = 'a', b = 'b', c = 'c';

    printf("變數 a : %c\n", a);
    printf("變數 b : %c\n", b);
    printf("變數 c : %c\n", c);

    // 參考: https://zh.wikipedia.org/wiki/ASCII

    printf("\n");
    printf("代表變數的代碼\n");
    printf("a 的代碼 : %d\n", a);
    printf("b 的代碼 : %d\n", b);
    printf("c 的代碼 : %d\n", c);

    printf("\n");
    printf("如果字元變數內儲存數字\n");

    char num1 = '0', num2 = '1';
    printf("變數 num1 : %c\n", num1);
    printf("變數 num2 : %c\n", num2);

    printf("\n");
    printf("代表數字字元的代碼\n");
    printf("num1 的代碼 : %d\n", num1);
    printf("num2 的代碼 : %d\n", num2);


    return 0;
}