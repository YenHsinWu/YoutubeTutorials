#include <stdio.h>

int main(){
    /*
    There are six comparison operators:
    1. > greater than
    2. < lesser than
    3. >= greater or equal
    4. <= lesser or equal
    5. == equal
    6. != non equal
    */

    int a = 10, b = 7;

    printf("a = %d, b = %d\n", a, b);
    printf("--------------------\n");

    printf("a > b is %d\n", a > b); // non zero
    printf("a < b is %d\n", a < b); // zero
    printf("a >= b is %d\n", a >= b); // non zero
    printf("a <= b is %d\n", a <= b); // zero
    printf("--------------------\n");

    a = 12;
    b = 12;
    printf("a = %d, b = %d\n", a, b);
    printf("--------------------\n");

    printf("a >= b is %d\n", a >= b); // non zero
    printf("a <= b is %d\n", a <= b); // non zero
    printf("a == b is %d\n", a == b); // non zero
    printf("a != b is %d\n", a != b); // zero

    return 0;
}