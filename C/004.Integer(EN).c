#include <stdio.h>

int main(){
    int a = 11, b = 5;

    /*
    int type supports five operators：
    1. '+' = addition
    2. '-' = substraction
    3. '*' = multiplication
    4. '/' = division
    5. '%' = modulo
    */

    int add, pos_sub, neg_sub, mul, div, mod;

    add = a + b; // = 16
    pos_sub = a - b; // = 6
    neg_sub = b - a; // = -6
    mul = a * b; // = 55
    div = a / b; // = 2
    mod = a % b; // = 1

    printf("The result of a + b : %d\n", add);
    printf("The result of a - b : %d\n", pos_sub);
    printf("The result of b - a : %d\n", neg_sub);
    printf("The result of a * b : %d\n", mul);
    printf("The result of a / b : %d\n", div);
    printf("The result of a %% b : %d\n", mod);
    printf("================================================\n");
    printf("Now turn a / b into b / a.......................\n");
    printf("The result of b / a : %d\n", b / a); // 5 / 11 = 0
    printf("The result of b %% a : %d\n", b % a); // 5 % 11 = 5

    return 0;
}
