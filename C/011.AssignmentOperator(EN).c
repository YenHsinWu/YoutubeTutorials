#include <stdio.h>

int main(){
    /*
    Assignment operator = is used to copy the value of RHS to the varialble at LHS.
    Ex:
    1) RHS is a value，store the value in LHS:a = 10;
    2) RHS is a variable, copy its value and store in LHS:b = 15; a = b; // a -> 15

    When coding, we update value in variable frequently, the syntax below is legal:
    a = a + 10;
    Since assignment operator copy the RHS to LHS, so the behavior of line 11 is:
    1) First do a + 10.
    2) Store the result of a + 10 into a.
    */

    int a = 5; //Assignment operator when initializing.
    int b;
    b = 15; //General assignment.

    printf("a = %d, b = %d\n", a, b);

    //Update variable a.
    a = a + 5; // a -> 10
    printf("Updated a = %d\n", a);

    /*
    Since the operation in line 24 is used frequently, we have below operators:
    1) a += 10 is same as a = a + 10
    2) a -= 10 is same as a = a - 10
    3) a *= 10 is same as a = a * 10
    4) a /= 10 is same as a = a / 10
    5) a %= 10 is same as a = a % 10 (Integer only)
    */

    //Update variable a.
    a += b; // a -> 25
    printf("Updated a = %d\n", a);

    /*
    When updating variable, a = a + 1 or a = a - 1 are used very frequently, thus:
    a ++ is same as a = a + 1
    b -- is same as b = b - 1
    ++, -- operators have different behavior when put before or after the variable.
    (Will explain in future.)
    */

    return 0;
}