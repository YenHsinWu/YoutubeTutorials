#include <stdio.h>

/*
while(條件){
    當條件為真，重複執行這一區塊
}
*/

int main(){
    int cnt, i = 0;

    printf("請輸入計數器的值: ");
    scanf("%d", &cnt);

    while(i < cnt){
        printf("%d ", i);
        i ++;
    }
    printf("\n");

    return 0;
}